package com.altuxo.emailmonitor.controler;

import com.altuxo.emailmonitor.dto.EmailDto;
import com.altuxo.emailmonitor.dto.EmailFromRepositoryDto;
import com.altuxo.emailmonitor.dtoconverter.Converter;
import com.altuxo.emailmonitor.enitites.Email;
import com.altuxo.emailmonitor.enitites.EmailStatus;
import com.altuxo.emailmonitor.service.EmailsRepositoryService;
import com.altuxo.emailmonitor.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import java.time.LocalDateTime;
import java.util.UUID;

@RestController
@RequestMapping("/emailmonitor")
public class EmailMonitorRestController {
    private static final Logger logger = LoggerFactory.getLogger(EmailMonitorRestController.class);

    private final EmailsRepositoryService emailsRepositoryService;
    private final Converter<Email, EmailDto, EmailFromRepositoryDto> converter;
    private final MailService mailService;

    public EmailMonitorRestController(final EmailsRepositoryService emailsRepositoryService, final Converter<Email, EmailDto, EmailFromRepositoryDto> converter, final MailService mailService) {
        this.emailsRepositoryService = emailsRepositoryService;
        this.converter = converter;
        this.mailService = mailService;
    }

    @GetMapping("/getemail/{id}")
    public ResponseEntity<EmailFromRepositoryDto> getEmailById(@PathVariable UUID id){
        logger.info("EmailMonitorRestController.getEmailById: [{}]", id);

        var result = emailsRepositoryService.getEmailById(id).map(converter::fromEntity);
        if(result.isPresent()){
            return ResponseEntity.ok(result.get());
        } else {
            logger.info("item not found [{}]", id);
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getemail/status/{id}")
    public ResponseEntity<String> getEmailStatusById(@PathVariable UUID id){
        logger.info("EmailMonitorRestController.getEmailById: [{}]", id);
        var result = emailsRepositoryService.getEmailStatusById(id);
        if(result.isPresent()){
            return ResponseEntity.ok(result.get().getStatus());
        } else {
            logger.info("item not found [{}]", id);
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/email/send")
    public EmailFromRepositoryDto sendEmail(@RequestBody EmailDto emailToSend){
        logger.info("Controller-saveNgo(), received param: [{}]", emailToSend);
        var email = converter.fromDto(emailToSend);

        try {
            mailService.sendMail(email);
            email.setDateTime(LocalDateTime.now());
            email.setStatus(EmailStatus.SENT.getDescription());
        } catch (SendFailedException sex){
            email.setDateTime(LocalDateTime.now());
            email.setStatus(EmailStatus.SEND_FAILED.getDescription());
            sex.printStackTrace();
        } catch (MessagingException e) {
            email.setDateTime(LocalDateTime.now());
            email.setStatus(EmailStatus.DELIVERY_FAILURE.getDescription());
            e.printStackTrace();
        } catch(Exception e) {
            email.setDateTime(LocalDateTime.now());
            email.setStatus(EmailStatus.DELIVERY_FAILURE.getDescription());
            e.printStackTrace();
        }
        finally {
            return converter.fromEntity(emailsRepositoryService.saveMail(email));
        }
    }
}
