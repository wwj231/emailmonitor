package com.altuxo.emailmonitor.dtoconverter;

public interface Converter <E, D, T>{
    T fromEntity(E entity);
    E fromDto(D dto);
}
