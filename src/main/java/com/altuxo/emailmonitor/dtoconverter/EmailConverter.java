package com.altuxo.emailmonitor.dtoconverter;

import com.altuxo.emailmonitor.dto.EmailDto;
import com.altuxo.emailmonitor.dto.EmailFromRepositoryDto;
import com.altuxo.emailmonitor.enitites.Email;
import org.springframework.stereotype.Component;

@Component
public class EmailConverter implements Converter<Email, EmailDto, EmailFromRepositoryDto> {
    @Override
    public EmailFromRepositoryDto fromEntity(final Email entity) {
        return EmailFromRepositoryDto.builder()
                .id(entity.getId().toString())
                .from(entity.getFrom())
                .to(entity.getTo())
                .dateTime(entity.getDateTime())
                .subject(entity.getSubject())
                .body(entity.getBody())
                .status(entity.getStatus())
                .build();
    }

    @Override
    public Email fromDto(final EmailDto dto) {
        return Email.builder()
                .from(dto.getFrom())
                .to(dto.getTo())
                .subject(dto.getSubject())
                .body(dto.getBody())
                .build();
    }
}
