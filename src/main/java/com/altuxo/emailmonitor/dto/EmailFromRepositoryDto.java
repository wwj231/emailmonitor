package com.altuxo.emailmonitor.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmailFromRepositoryDto {

    private String id;

    private String from;

    private String to;

    private LocalDateTime dateTime;

    private String subject;

    private String body;

    private String status;
}
