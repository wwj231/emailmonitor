package com.altuxo.emailmonitor.repository;

import com.altuxo.emailmonitor.enitites.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface EmailsRepository extends JpaRepository<Email, UUID> {
}
