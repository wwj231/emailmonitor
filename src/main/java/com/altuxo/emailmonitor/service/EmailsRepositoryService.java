package com.altuxo.emailmonitor.service;

import com.altuxo.emailmonitor.enitites.Email;
import com.altuxo.emailmonitor.repository.EmailsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class EmailsRepositoryService {
    private static final Logger logger = LoggerFactory.getLogger(EmailsRepositoryService.class);
    private final EmailsRepository emailsRepository;

    public EmailsRepositoryService(final EmailsRepository emailsRepository) {
        this.emailsRepository = emailsRepository;
    }

    public Optional<Email> getEmailById(UUID uuid){
        logger.info("EmailsRepositoryService.getEmailById : [{}]", uuid.toString());
        var foundEmail = emailsRepository.findById(uuid);
        return foundEmail;
    }

    public Optional<Email> getEmailStatusById(UUID uuid){
        logger.info("EmailsRepositoryService.getEmailStatusById : [{}]", uuid.toString());
        var foundEmail = emailsRepository.findById(uuid);
        return foundEmail;
    }

    public Email saveMail(final Email email){
        logger.info("EmailsRepositoryService.savingMail : [{}]", email);
        var savedMail = emailsRepository.save(email);
        logger.info("Item saved : [{}]", savedMail);
        return savedMail;
    }
}
