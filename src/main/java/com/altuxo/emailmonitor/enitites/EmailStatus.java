package com.altuxo.emailmonitor.enitites;

public enum EmailStatus {
    SENT("sent"),
    DELIVERED("delivered"),
    SEND_FAILED("send_failed : 550"),
    DELIVERY_FAILURE("delivery_failure");

    private final String description;

    EmailStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
